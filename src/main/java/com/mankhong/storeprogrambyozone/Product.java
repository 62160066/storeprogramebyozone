/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.storeprogrambyozone;

import java.io.Serializable;

/**
 *
 * @author W.Home
 */
public class Product implements Serializable {
    String name,brand;
    int amount;
    int id = 0;
    double price;
    Product(int id,String name,String brand,double price, int amount){
        this.name  = name;
        this.brand = brand;
        this.price = price;
        this.amount = amount;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public  int getId() {
        return id;
    }

    public  void setId(int id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    public void ResetId(){
        id = 0 ;
    }
    public String toString(){
        return "[   id : "+id+" name : "+name+" Brand : "+brand+" Price : "+price+" Amount : "+amount+"  ]";
    }
}
