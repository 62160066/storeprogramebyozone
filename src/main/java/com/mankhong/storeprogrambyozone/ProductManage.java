/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.storeprogrambyozone;

/**
 *
 * @author W.Home
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductManage {
      private static ArrayList<Product> productList = new ArrayList<>();
      private static Product currentProduct = null ;
      static{
          load();
         
      }
      public static boolean addProduct(Product product){
          productList.add(product);
          save();
          return true;
      }
      public static boolean UpdateProduct(int index,Product product){
          productList.set(index , product);
          save();
          return true;
      }
      public static  Product getProductList(int index) {
        return productList.get(index);
      }
      public static boolean delProduct(int index){
          productList.remove(index);
          save();
          return true;
      }
      public static ArrayList<Product> getProduct(){
        return productList;
      }
      public static boolean clearProduct(){
          productList.clear();
          save();          
          return true;
      }
      public static int getSize(){
          return productList.size();
      }
      public static int getIndex(int index){
          return index;
      }
      public static String total(){
          double totalPrice = 0;
          int totalAmount = 0;
          for(int i = 0 ; i < productList.size() ; i++){
              totalPrice = totalPrice + productList.get(i).getPrice();
              totalAmount = totalAmount + productList.get(i).getAmount();                      
          }
          return "Price = "+totalPrice+" Amount = "+totalAmount;
      }
      public static void save(){
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try{
            file = new File("OZONE.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex){
            Logger.getLogger(ProductManage.class.getName()).log(Level.SEVERE,
                    null,ex );
        }catch (IOException ex){
            Logger.getLogger(ProductManage.class.getName()).log(Level.SEVERE,
                    null,ex );
        }
      }
      public static void load(){
         File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try{
            file = new File("OZONE.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>)ois.readObject();
            ois.close();
            fis.close();
        }catch (FileNotFoundException ex){
            Logger.getLogger(ProductManage.class.getName()).log(Level.SEVERE
           ,null,ex);
        }catch (IOException ex){
            Logger.getLogger(ProductManage.class.getName()).log(Level.SEVERE
           ,null,ex);
        }catch (ClassNotFoundException ex){
            Logger.getLogger(ProductManage.class.getName()).log(Level.SEVERE
           ,null,ex);
        }
          
      }
      
}
